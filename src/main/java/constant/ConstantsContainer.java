package constant;

public class ConstantsContainer {
    public static final String READ_JSON_PATH = "src/main/resources/devices.json";
    public static final String WRITE_JSON_PATH = "src/main/resources/writtenDevices.json";
    public static final String WRITE_JSON_PATH_SORTED = "src/main/resources/WrittenDevicesSorted.json";

    private ConstantsContainer() {
    }
}
