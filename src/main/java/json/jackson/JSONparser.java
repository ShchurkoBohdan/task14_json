package json.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Computer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class JSONparser {
    public static Logger logger = LogManager.getLogger(JSONparser.class);
    public static ObjectMapper objectMapper = new ObjectMapper();

    private JSONparser() {
    }

    public static List<Computer> parseJSON(File json) {
        Computer[] tempArray = new Computer[0];
        try {
            tempArray = objectMapper.readValue(json, Computer[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Arrays.asList(tempArray);
    }

    public static void print(List<Computer> computers) {
        for (Computer i : computers) {
            logger.info(i.print());
        }
    }
}
