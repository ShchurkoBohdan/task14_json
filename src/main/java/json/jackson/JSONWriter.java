package json.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import constant.ConstantsContainer;
import model.Computer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class JSONWriter {
    public static Logger logger = LogManager.getLogger(JSONparser.class);
    public static ObjectMapper objectMapper = new ObjectMapper();

    private JSONWriter() {
    }

    public static void writeJSONfromObj(List<Computer> computers, boolean sortedList) {
        try {
            if (sortedList){
                objectMapper.writeValue(new File(ConstantsContainer.WRITE_JSON_PATH_SORTED), computers);
            }else {
                objectMapper.writeValue(new File(ConstantsContainer.WRITE_JSON_PATH), computers);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<Computer> sortList(List<Computer> computers){
        List<Computer> sortedList = computers.stream().sorted(Comparator.comparing(Computer::getName)).collect(Collectors.toList());
        return sortedList;
    }
}
