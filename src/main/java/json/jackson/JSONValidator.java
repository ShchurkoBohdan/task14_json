package json.jackson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.File;
import java.io.IOException;

public class JSONValidator {
    public static Logger logger = LogManager.getLogger(JSONparser.class);
    public static ObjectMapper objectMapper = new ObjectMapper();

    public static boolean isValidJSON(File json) throws IOException {
        boolean valid = true;
        try {
            objectMapper.readTree(json);
        } catch (JsonProcessingException e) {
            valid = false;
            e.printStackTrace();
        }
        logger.info("\nThis JSON is valid - " + valid);
        return valid;

    }
}
