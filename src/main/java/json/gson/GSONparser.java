package json.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import model.Computer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;

public class GSONparser {
    public static Logger logger = LogManager.getLogger(GSONparser.class);
    public static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    private GSONparser() {
    }

    public static Computer[] parseJson(File file) {
        Computer[] computers = new Computer[0];
        try {
            computers = gson.fromJson(new BufferedReader(new FileReader(file)), Computer[].class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return computers;
    }

    public static void print(Computer[] arr) {
        List<Computer> computers = Arrays.asList(arr);
        for (Computer i : computers) {
            logger.info(i.print());
        }
    }
}
