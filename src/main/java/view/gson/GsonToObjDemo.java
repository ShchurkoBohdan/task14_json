package view.gson;

import constant.ConstantsContainer;
import json.gson.GSONparser;
import java.io.File;

public class GsonToObjDemo {
    public static void main(String[] args) {
        GSONparser.print(GSONparser.parseJson(new File(ConstantsContainer.READ_JSON_PATH)));
    }
}
