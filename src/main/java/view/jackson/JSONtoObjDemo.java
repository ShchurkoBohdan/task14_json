package view.jackson;

import constant.ConstantsContainer;
import json.jackson.JSONparser;
import java.io.File;

public class JSONtoObjDemo {
    public static void main(String[] args) {
        JSONparser.print(JSONparser.parseJSON(new File(ConstantsContainer.READ_JSON_PATH)));
    }
}
