package view.jackson;

import constant.ConstantsContainer;
import json.jackson.JSONWriter;
import json.jackson.JSONparser;
import java.io.File;

public class ObjToJSONDemo {
    public static void main(String[] args) {
        JSONWriter.writeJSONfromObj(JSONparser.parseJSON(new File(ConstantsContainer.READ_JSON_PATH)), false);
    }
}
