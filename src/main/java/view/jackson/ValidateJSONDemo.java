package view.jackson;

import constant.ConstantsContainer;
import json.jackson.JSONValidator;
import java.io.File;
import java.io.IOException;

public class ValidateJSONDemo {
    public static void main(String[] args) {
        try {
            JSONValidator.isValidJSON(new File(ConstantsContainer.READ_JSON_PATH));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
