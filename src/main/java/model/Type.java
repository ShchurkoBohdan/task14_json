package model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Type {
    @JsonProperty("isPeripheral")
    private boolean isPeripheral;
    private int energyConsumption;
    private boolean hasCooler;
    private Accessory[] accessories;
    private Port[] ports;

    public Type() {
    }

    public Type(boolean isPeripheral, int energyConsumption, boolean hasCooler, Accessory[] accessories, Port[] ports) {
        this.isPeripheral = isPeripheral;
        this.energyConsumption = energyConsumption;
        this.hasCooler = hasCooler;
        this.accessories = accessories;
        this.ports = ports;
    }

    public boolean isPeripheral() {
        return isPeripheral;
    }

    public void setPeripheral(boolean isPeripheral) {
        this.isPeripheral = isPeripheral;
    }

    public int getEnergyConsumption() {
        return energyConsumption;
    }

    public void setEnergyConsumption(int energyConsumption) {
        this.energyConsumption = energyConsumption;
    }

    public boolean isHasCooler() {
        return hasCooler;
    }

    public void setHasCooler(boolean hasCooler) {
        this.hasCooler = hasCooler;
    }

    public Accessory[] getAccessories() {
        return accessories;
    }

    public void setAccessories(Accessory[] accessories) {
        this.accessories = accessories;
    }

    public Port[] getPorts() {
        return ports;
    }

    public void setPorts(Port[] ports) {
        this.ports = ports;
    }
}
