package model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Computer implements Comparable<Computer>{
    private String name;
    private String origin;
    private int price;
    private Type type;
    private boolean isCritical;

    public Computer() {
    }

    public Computer(String name, String origin, int price, Type type, boolean isCritical) {
        this.name = name;
        this.origin = origin;
        this.price = price;
        this.type = type;
        this.isCritical = isCritical;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public boolean isCritical() {
        return isCritical;
    }

    public void setCritical(boolean critical) {
        isCritical = critical;
    }

    public String print() {
        return "\nOBJECT - " + this.getClass() +
                "\nName - " + getName() +
                "\nOrigin - " + getOrigin() +
                "\nPrice - " + getPrice() +
                "\nType: " +
                "\n\tIs Peripheral - " + getType().isPeripheral() +
                "\n\tEnergy Consumption - " + getType().getEnergyConsumption() +
                "\n\tHas cooler - " + getType().isHasCooler() +
                "\n\tAccessories: " + getAccessoriesNames() +
                "\n\tPorts: " + getPortNames();
    }

    @JsonIgnore
    public String getAccessoriesNames() {
        String accessories = "";
        Accessory[] accArr = getType().getAccessories();
        for (int i = 0; i < accArr.length; i++) {
            accessories += "\n\t\tAccessory - " + accArr[i].getAccessoryName();
        }
        return accessories;
    }

    @JsonIgnore
    public String getPortNames() {
        String portNames = "";
        Port[] ports = getType().getPorts();
        for (int i = 0; i < ports.length; i++) {
            portNames += "\n\t\t Port - " + ports[i].getPortName();
        }
        return portNames;
    }

    @Override
    public int compareTo(Computer computer) {
        if (this.getName() != null && computer.getName() != null ){
            return this.getName().compareToIgnoreCase(computer.getName());
        }else {
            return 0;
        }
    }
}
